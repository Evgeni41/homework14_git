﻿
#include <iostream>

int main()
{
    std::string str = "Wonderful!";
    //для переноса на следующую строку можно использовать std::endl 
    std::cout << str << "\n";
    std::cout << "String length: " << str.length() << "\n";
    std::cout << "First symbol: " << str.front() << "\n"; //str.front() эквивалентно str[0]
    std::cout << "Last symbol: " << str.back() << "\n"; //str.back эквивалентно str[str.size() - 1]
    return 0;
   
}


